package opsfile

import (
	//"fmt"
	"os"

	"gitlab.com/olanguage/olang/proc"
	"gopkg.in/ini.v1"

	//"syscall"
	//"github.com/olproject/ops/pid"
	"log"
)

type Opsfile struct {
	Filename string
	Pid      int64
	Result   string
	Proc     *os.Process
}

func New(filename string) *Opsfile {
	return &Opsfile{
		Filename: filename,
		Pid:      0,
	}
}

func (of *Opsfile) Run() {
	cfg, err := ini.Load(of.Filename)
	if err != nil {
		log.Printf("Fail to read file: %v", err)
		os.Exit(1)
	}

	//cpu, _ := cfg.Section("config").Key("cpu").Int64()
	//ram, _ := cfg.Section("config").Key("ram").Int64()
	procs := cfg.Section("olang").Keys()

	for _, procItem := range procs {
		proc := proc.New(procItem.Name(), "olang", procItem.Value())
		proc.Execute()
	}
}
