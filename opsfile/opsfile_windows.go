package opsfile

import (
	"fmt"
	"os"

	//"gopkg.in/ini.v1"
	//"github.com/olproject/ops/proc"
	//"syscall"
	"log"
	"os/exec"

	"gitlab.com/olanguage/olang/pid"
)

func (of *Opsfile) Kill() bool {
	if _, err := os.Stat(".oproc"); os.IsNotExist(err) {
		log.Printf("Pid folder not found!")
		os.Exit(0)
	}

	files, err := os.ReadDir(".oproc/")
	if err != nil {
		log.Fatal(err)
	}

	for _, file := range files {
		id, err := pid.GetValue(".oproc/" + file.Name())
		if err != nil {
			log.Printf("Process not found.\n")
			os.Exit(1)
		}

		log.Printf("Process stoped: %d\n", id)
		exec.Command("taskkill", "/F", "/T", "/PID", fmt.Sprint(id)).Run()
		os.Remove(".oproc/" + file.Name())
	}

	return true
}
