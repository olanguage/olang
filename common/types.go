package common

import (
	"fmt"
	"gitlab.com/olanguage/olang/object"
)

// Error handling functions
func NewError(format string, a ...interface{}) *object.Error {
	return &object.Error{Message: fmt.Sprintf(format, a...)}
}

func NewWarning(format string, a ...interface{}) *object.Warning {
	return &object.Warning{Message: fmt.Sprintf(format, a...)}
} 