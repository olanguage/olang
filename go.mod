module gitlab.com/olanguage/olang

go 1.16

require (
	github.com/Microsoft/go-winio v0.5.1 // indirect
	github.com/buger/jsonparser v1.1.1
	github.com/clbanning/mxj v1.8.4
	github.com/cpuguy83/go-md2man/v2 v2.0.1 // indirect
	github.com/davecgh/go-spew v1.1.1
	github.com/denisenkom/go-mssqldb v0.12.0
	github.com/fsnotify/fsnotify v1.5.1
	github.com/gin-gonic/gin v1.7.7
	github.com/go-playground/validator/v10 v10.10.0 // indirect
	github.com/go-sql-driver/mysql v1.6.0
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/golang/snappy v0.0.4 // indirect
	github.com/gorilla/mux v1.8.0
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/julienschmidt/httprouter v1.3.0
	github.com/kevinburke/ssh_config v1.1.0 // indirect
	github.com/lib/pq v1.10.4
	github.com/mattn/go-colorable v0.1.12
	github.com/mattn/go-isatty v0.0.14
	github.com/mattn/go-sqlite3 v1.14.11
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/oytunistrator/asm v0.0.0-20180810134229-fdb6eaeba5f8
	github.com/sergi/go-diff v1.2.0 // indirect
	github.com/skratchdot/open-golang v0.0.0-20200116055534-eef842397966
	github.com/syndtr/goleveldb v1.0.0
	github.com/ugorji/go v1.2.6 // indirect
	github.com/urfave/cli v1.22.5
	github.com/xanzy/ssh-agent v0.3.1 // indirect
	gitlab.com/olanguage/ops v0.0.0-20181204092000-e82b6c971e09
	golang.org/x/crypto v0.0.0-20220128200615-198e4374d7ed // indirect
	golang.org/x/net v0.0.0-20220127200216-cd36cc0744dd // indirect
	golang.org/x/sys v0.0.0-20220310020820-b874c991c1a5
	golang.org/x/text v0.3.7 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	google.golang.org/protobuf v1.27.1 // indirect
	gopkg.in/ini.v1 v1.66.3
	gopkg.in/src-d/go-git.v4 v4.13.1
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
