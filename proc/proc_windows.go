package proc

func (p *Proc) RunCommand(ch chan<- Data) {
	//cmd := exec.Command(p.Command, p.Arg, "&")
	cmd := exec.Command("start", p.Command+" "+p.Arg)
	data, err := cmd.CombinedOutput()
	ch <- Data{
		error:  err,
		output: data,
		exec:   cmd,
	}
}
