package repl

import (
	_"bufio"
	"fmt"
	"io"
	"os"
	"strings"

	"gitlab.com/olanguage/olang/color"
	"gitlab.com/olanguage/olang/compiler"
	"gitlab.com/olanguage/olang/evaluator"
	"gitlab.com/olanguage/olang/lexer"
	"gitlab.com/olanguage/olang/object"
	"gitlab.com/olanguage/olang/parser"
	"gitlab.com/olanguage/olang/systemlib"
	"gitlab.com/olanguage/olang/vm"
	"gitlab.com/olanguage/olang/token"
	"gitlab.com/olanguage/olang/watcher"
	
	"gitlab.com/olanguage/olang/version"
	"gitlab.com/olanguage/olang/readline"

)

var PROMPT = "olang-"+version.Version()+"> "


func GetBuiltins() string {
	// Key'leri bir slice'a ekleyelim
	keys := make([]string, 0, len(evaluator.PreBuiltFunctions))
	for key := range evaluator.PreBuiltFunctions {
		keys = append(keys, key+"()")
	}

	// Slice'ı joinleyerek tek bir string haline getirelim
	joinedKeys := strings.Join(keys, ", ")

	return fmt.Sprintf("Functions: \n\t%s", joinedKeys)
}

func GetTokens() string {
	// Key'leri bir slice'a ekleyelim
	keys := make([]string, 0, len(token.PreBuiltTokens))
	for key := range token.PreBuiltTokens {
		keys = append(keys, key)
	}

	// Slice'ı joinleyerek tek bir string haline getirelim
	joinedKeys := strings.Join(keys, ", ")

	return fmt.Sprintf("Tokens: \n\t%s", joinedKeys)
}

func printHelp() {
	fmt.Println("O Programming Language (olang)")
	fmt.Println("")
	fmt.Println(version.FullInfo())
	fmt.Println("")
	fmt.Println("System Oriented Functional Programming Language (SOFPL) is a specific structure that interprets each line separately and outputs the result. Shorten the functions performed in normal programming languages.")
	fmt.Println("")
	fmt.Println(GetTokens())
	fmt.Println("")
	fmt.Println(GetBuiltins())
	fmt.Println("")
	fmt.Println("Documentation:\n\tonix-project.com/O_Language")
	fmt.Println("Gitlab:\n\tgitlab.com/olanguage/olang")
	fmt.Println("")
}



func Start(in io.Reader, out io.Writer, debug bool) {
	promptVar := color.Green(PROMPT)
	getPromptEnv := os.Getenv("OLANG_PROMPT")
	if getPromptEnv != "" {
		promptVar = getPromptEnv
	}
	//scanner := bufio.NewScanner(in)
	env := object.NewEnvironment()

	for {
		rl, err := readline.New(promptVar)
		if err != nil {
			fmt.Println(err)
			return
		}
		defer rl.Close()
		line, err := rl.Readline()
        if err != nil {
            fmt.Println(err)
            return
        }

		switch line{
		case "help":
			printHelp()
		case "version":
			fmt.Println(version.FullInfo())
		case "exit":
			os.Exit(1)
		default:
			l := lexer.New(line)
			p := parser.New(l)

			program := p.ParseProgram()
			if len(p.Errors()) != 0 {
				printParserErrors(out, p.Errors())
				continue
			}

			evaluated := evaluator.Eval(program, env)

			if evaluated != nil {
				if evaluated.Result() != "" {
					io.WriteString(out, evaluated.Result())
					io.WriteString(out, "\n")
				}
			}
		}

		
	}
}


func FileCompile(filename string, out io.Writer, debug bool) {
	content, err := os.ReadFile(filename)
	constants := []object.Object{}
	//globals := make([]object.Object, vm.GlobalsSize)
	symbolTable := compiler.NewSymbolTable()

	if err != nil {
		printParserError(out, err)
	}

	contstr := string(content)

	if len(contstr) > 0 {

		l := lexer.New(contstr)
		p := parser.New(l)

		program := p.ParseProgram()
		if len(p.Errors()) != 0 {
			printParserErrors(out, p.Errors())
		}

		comp := compiler.NewWithState(symbolTable, constants)
		err := comp.Compile(program)
		if err != nil {
			fmt.Fprintf(out, "Compilation failed:\n %s\n", err)
		}

		code := comp.Bytecode()
		print(code.Instructions.String())
		constants = code.Constants

		machine := vm.New(code)
		err = machine.Run()
		if err != nil {
			fmt.Fprintf(out, "Executing bytecode failed:\n %s\n", err)
		}

		stackTop := machine.LastPoppedStackElement()
		if stackTop != nil {
			io.WriteString(out, stackTop.Inspect())
			io.WriteString(out, "\n")
		}

	}
}

func StringParse(input string, out io.Writer, debug bool) {
	env := object.NewEnvironment()

	contstr := string(input)

	if len(contstr) > 0 {

		l := lexer.New(contstr)
		p := parser.New(l)

		program := p.ParseProgram()

		if len(p.Errors()) != 0 {
			printParserErrors(out, p.Errors())
			if debug {
				fmt.Printf("Stoped Line:\n")
				fmt.Printf("%#v\n", p.LexerReturn())
			}
			return
		}

		evaluated := evaluator.Eval(program, env)

		if evaluated != nil {
			if evaluated.Result() != "" {
				io.WriteString(out, evaluated.Result())
				if debug {
					fmt.Printf("\n%#v\n", evaluated)
				}
			}
		}
	}
}

func FileParse(filename string, out io.Writer, debug bool, live bool, libraries systemlib.Libraries) {
	env := object.NewEnvironment()

	contstr := ""
	for _, lib := range libraries {
		content, err := os.ReadFile(lib.File)
		w := watcher.New()
		if err != nil {
			printParserError(out, err)
		}

		if live {
			w.Add(filename)
		}
		contstr += string(content)
	}

	content, err := os.ReadFile(filename)
	w := watcher.New()
	if err != nil {
		printParserError(out, err)
	}

	if live {
		w.Add(filename)
	}

	contstr += string(content)

	if len(contstr) > 0 {

		l := lexer.New(contstr)
		p := parser.New(l)

		program := p.ParseProgram()

		if len(p.Errors()) != 0 {
			printParserErrors(out, p.Errors())
			if debug {
				fmt.Printf("Stoped Line:\n")
				fmt.Printf("%#v\n", p.LexerReturn())
				fmt.Printf("%#v\n", p.ErrorTokens())
			}
			return
		}

		evaluated := evaluator.Eval(program, env)

		if evaluated != nil {
			if evaluated.Result() != "" {
				io.WriteString(out, evaluated.Result())
				if debug {
					fmt.Printf("\n%#v\n", evaluated)
				}
			}
		}

		if live {
			w.Run(func() {
				FileParse(filename, out, debug, live, libraries)
			})
		}
	}
}

func printParserError(out io.Writer, err error) {
	io.WriteString(out, color.Bold(color.Red("Your code not valid!\n")))
	io.WriteString(out, color.Bold(color.Blue("  syntax errors:\n")))

	io.WriteString(out, "\t"+fmt.Sprintf("%s", err)+"\n")
}

func printParserErrors(out io.Writer, errors []string) {
	io.WriteString(out, color.Bold(color.Red("Your code not valid!\n")))
	io.WriteString(out, color.Bold(color.Blue("  syntax errors:\n")))

	for _, msg := range errors {
		io.WriteString(out, "\t"+msg+"\n")
	}
}

