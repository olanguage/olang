package color

import (
	"fmt"
)

type Colorize struct{
	text string
	color string
	background bool

}

func Create(text string, color string, background bool) *Colorize {
	return &Colorize{
		text: text,
		color: color,
		background: background,
	}
}

func (c *Colorize) ShowLn(){
	switch c.color {
		case "black":
			if c.background == true{
				fmt.Println(BlackBg(c.text))
			}else{
				fmt.Println(Black(c.text))
			}
		case "red":
			if c.background == true{
				fmt.Println(RedBg(c.text))
			}else{
				fmt.Println(Red(c.text))
			}
		case "green":
			if c.background == true{
				fmt.Println(GreenBg(c.text))
			}else{
				fmt.Println(Green(c.text))
			}
		case "yellow":
			if c.background == true{
				fmt.Println(YellowBg(c.text))
			}else{
				fmt.Println(Yellow(c.text))
			}
		case "blue":
			if c.background == true{
				fmt.Println(BlueBg(c.text))
			}else{
				fmt.Println(Blue(c.text))
			}
		case "magenta":
			if c.background == true{
				fmt.Println(MagentaBg(c.text))
			}else{
				fmt.Println(Magenta(c.text))
			}
		case "cyan":
			if c.background == true{
				fmt.Println(CyanBg(c.text))
			}else{
				fmt.Println(Cyan(c.text))
			}
		case "white":
			if c.background == true{
				fmt.Println(WhiteBg(c.text))
			}else{
				fmt.Println(White(c.text))
			}
		case "grey":
			if c.background == true{
				fmt.Println(Grey(c.text))
			}else{
				fmt.Println(Grey(c.text))
			}
		default:
			fmt.Println(c.text)
	}
}


func (c *Colorize) StdOut() string {
	switch c.color {
	case "black":
		if c.background == true{
			return BlackBg(c.text)
		}else{
			return Black(c.text)
		}
	case "red":
		if c.background == true{
			return RedBg(c.text)
		}else{
			return Red(c.text)
		}
	case "green":
		if c.background == true{
			return GreenBg(c.text)
		}else{
			return Green(c.text)
		}
	case "yellow":
		if c.background == true{
			return YellowBg(c.text)
		}else{
			return Yellow(c.text)
		}
	case "blue":
		if c.background == true{
			return BlueBg(c.text)
		}else{
			return Blue(c.text)
		}
	case "magenta":
		if c.background == true{
			return MagentaBg(c.text)
		}else{
			return Magenta(c.text)
		}
	case "cyan":
		if c.background == true{
			return CyanBg(c.text)
		}else{
			return Cyan(c.text)
		}
	case "white":
		if c.background == true{
			return WhiteBg(c.text)
		}else{
			return White(c.text)
		}
	case "grey":
		if c.background == true{
			return Grey(c.text)
		}else{
			return Grey(c.text)
		}
	default:
		return c.text
	}
	
	return c.text
}