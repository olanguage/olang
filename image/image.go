package image

import (
	"fmt"
	"image"
	_ "image/gif"
	_ "image/jpeg"
	_ "image/png"
	"os"

	"gitlab.com/olanguage/olang/file"
)

type Image struct {
	Name    string
	Size    int
	Type    string
	Content []byte
	Decoded image.Image
}

func New(dataOrFile interface{}) *Image {
	switch d := dataOrFile.(type) {
	case string:
		imageFile := file.New(d)
		reader, err := os.Open(imageFile.Name)
		if err != nil {
			newError("file cannot open", dataOrFile)
		}
		defer reader.Close()
		imgData, _, _ := image.Decode(reader)
		return &Image{
			Name:    imageFile.Name,
			Size:    imageFile.Size,
			Type:    imageFile.Type,
			Content: imageFile.Content,
			Decoded: imgData,
		}
	default:
		newError("type not supported", dataOrFile)
	}

	return nil
}

func newError(format string, a ...interface{}) {
	fmt.Printf(format, a...)
}
