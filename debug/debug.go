package debug

import (
	"fmt"
)

type Debugger struct {
	Symbol interface{}
}

func New() *Debugger {
	return &Debugger{}
}

func (d *Debugger) PrintInterface(symbol interface{}) {
	d.Symbol = symbol
	fmt.Printf("\n%#v\n", d.Symbol)
}

func (d *Debugger) SprintInterface(symbol interface{}) string {
	d.Symbol = symbol
	return fmt.Sprintf("\n%#v\n", d.Symbol)
}

func (d *Debugger) Print() {
	fmt.Printf("\n%#v\n", d.Symbol)
}
