package version

import (
	"fmt"
)

// Bu değişkenler ldflags ile derleme sırasında set edilecek
var (
	version   = "latest"       // Git tag veya versiyon bilgisi
	commit    = "master"      // Git commit hash bilgisi
	date      = ""   // Build zamanı
)

// Versiyon bilgilerini döndüren bir fonksiyon
func FullInfo() string {
	return fmt.Sprintf("Version: %s\nCommit: %s\nBuild Date: %s\n", version, commit, date)
}

func Version() string {
	return fmt.Sprintf("%s", version)
}

func Commit() string {
	return fmt.Sprintf("%s", commit)
}

func Date() string {
	return fmt.Sprintf("%s", date)
}