package version

import (
	"fmt"
	"os/exec"
	"strings"
)

// Git komutunu çalıştıran yardımcı fonksiyon
func getGitOutput(args ...string) string {
	cmd := exec.Command("git", args...)
	out, err := cmd.Output()
	if err != nil {
		return ""
	}
	return strings.TrimSpace(string(out))
}

// Git tag (versiyon) bilgisini döner
func GitVersion() string {
	return getGitOutput("describe", "--tags")
}

// Git commit hash bilgisini döner
func GitCommit() string {
	return getGitOutput("rev-parse", "HEAD")
}

// Build zamanını git'ten almak zor olabilir, bunun yerine `date` komutunu kullanabiliriz
func GitDate() string {
	return getGitOutput("log", "-1", "--format=%cd", "--date=iso")
}

// Tüm versiyon bilgilerini döndüren fonksiyon
func GitFullInfo() string {
	return fmt.Sprintf("Version: %s\nCommit: %s\nBuild Date: %s\n", Version(), Commit(), Date())
}
