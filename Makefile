.PHONY: build


build:
	go get -v .
	go build -ldflags "-X gitlab.com/olanguage/olang/version.version=$$(git describe --tags) -X gitlab.com/olanguage/olang/version.commit=$$(git rev-parse HEAD) -X gitlab.com/olanguage/olang/version.date=$$(date -u +%Y-%m-%dT%H:%M:%SZ)" -o olang

install:
	go install -ldflags "-X gitlab.com/olanguage/olang/version.version=$$(git describe --tags)" gitlab.com/olanguage/olang@latest


docker:
	/bin/bash _scripts/docker-build.sh

snap-build:
	sudo snapcraft
	snapcraft upload --release=stable olang*.snap