#!/bin/bash
DOCKER_REPO="olproject/olang"

if [ -z $1 ]; then
DOCKER_REPO=$DOCKER_REPO
else
DOCKER_REPO=$DOCKER_REPO:$1
fi

docker build -t $DOCKER_REPO .
docker push $DOCKER_REPO
