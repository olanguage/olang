FROM golang

RUN go install -v -ldflags "-X gitlab.com/olanguage/olang/version.version=docker" gitlab.com/olanguage/olang@latest

CMD ["olang"]
